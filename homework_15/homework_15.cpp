﻿// homework_15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

const int N = 100;

void print_OddEvenSeq(int N, bool isOdd) {
    int endLoop = N / 2 - isOdd * ((N + 1) % 2);
    for (int i = 0; i <= endLoop; i++)
        std::cout << 2 * i + isOdd << '\n';
}

int main()
{
    int n = 0;
    std::cout << "Even numbers:\n";
    for (int i = 0; i <= N/2; i++)
        std::cout << 2 * i << "\n";
    
    print_OddEvenSeq(20, false);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
